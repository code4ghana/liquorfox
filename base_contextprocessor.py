from liquorfox_core.models import Beverages
from django.core.cache import cache

def beverage_typeprocessor(request):
    cache_key='beverage_types'
    together=cache.get(cache_key)
    if together:
        return {'types':together,}
    types=[ b.values()[0] for b in Beverages.objects.values('type1').distinct()]
    together={b:(Beverages.objects.filter(type1=b).count()) for b in types}
    cache.set(cache_key,together)
    return {'types':together}
