"""
URLConf for Django user profile management.

Recommended usage is to use a call to ``include()`` in your project's
root URLConf to include this URLConf for any URL beginning with
'/profiles/'.

If the default behavior of the profile views is acceptable to you,
simply use a line like this in your root URLConf to set up the default
URLs for profiles::

    (r'^profiles/', include('profiles.urls')),

But if you'd like to customize the behavior (e.g., by passing extra
arguments to the various views) or split up the URLs, feel free to set
up your own URL patterns for these views instead. If you do, it's a
good idea to keep the name ``profiles_profile_detail`` for the pattern
which points to the ``profile_detail`` view, since several views use
``reverse()`` with that name to generate a default post-submission
redirect. If you don't use that name, remember to explicitly pass
``success_url`` to those views.

"""

from django.conf.urls.defaults import url,patterns


from profiles import views


urlpatterns = patterns('',
                       url(r'^create/$',
                           views.create_profile,
                           {'template_name':'registration/create_profile.html',
                           'backend':'registration.backend'},
                           name='profiles_create_profile'),
                       url(r'^edit/$',
                           views.edit_profile,
                           {'template_name':'registration/edit_profile.html'},
                           name='profiles_edit_profile'),
                       url(r'^(?P<username>\w+)/$',
                           views.profile_detail,
                           {'template_name':'registration/view_profile.html'},
                           name='profiles_profile_detail'),
                       url(r'^$',
                           views.profile_list,
                           {'template_name':'registration/profiles_list.html'},
                           name='profiles_profile_list'),
                       )
