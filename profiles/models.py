# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from userena.models import UserenaBaseProfile


# When model instance is saved, trigger creation of corresponding profile



# definition of UserProfile from above
# ..

class Profile(UserenaBaseProfile):
    # This field is required.
    user = models.OneToOneField(User,unique=True,verbose_name='user',related_name='my_profile')
    # must do age verification
    dob=models.DateField(blank=True, null=True)
    GENDER_CHOICES=(('M','Male'),('F','Female'))
    sex=models.CharField(max_length=1,choices=GENDER_CHOICES,blank=True)
    
    def get_absolute_url(self):
        return ('profiles_profile_detail', (), { 'username': self.user.username })
    get_absolute_url = models.permalink(get_absolute_url)
