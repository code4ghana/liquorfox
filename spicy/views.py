from liquorfox_core.models import Beverages,Products,Sells
from django.shortcuts import render,redirect
from django.db.models import Min,Max
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from itertools import chain
#import pdb
DEFAULT_PAGE=1
DEFAULT_LIMIT=10
sort_map={'a_z':'name','z_a':'-name','price_low':'t_price','price_high':'-t_price'}
bev_field_mapping={'name':'name','pic_url':'pic_url','description':'description','type1':'type1','id':'beverage_id'}
values=('name','pic_url','description','type1','id')


def orderBy(items,criteria):

    reg_sorting={}
    
    if(criteria in opt_sorting):
        items=items.order_by(opt_sorting[criteria])
        return items
    items=items.order_by('name')
    #mark as sorted
    return items

def category(request,type1):
    sorting=request.REQUEST.get('sort_by')
    sorting=sort_map.get(sorting,'name')
    page=request.REQUEST.get('page',DEFAULT_PAGE)
    show=request.REQUEST.get('show',DEFAULT_LIMIT)
    show=show if show in {10,20,50,100} else DEFAULT_LIMIT
    if show>100:
        show=100
    #selects all products of type1, return the lowest_price for unique beverages
    prods=Products.objects.defer('id').select_related(*bev_field_mapping.values()).filter(beverage__type1__iexact=type1).extra(select=bev_field_mapping)
    prods=prods.values(*values).annotate(t_price=Min('price'))
    bevs=Beverages.objects.filter(type1__iexact=type1, products__isnull=True).values(*values)
    results=[]
    #pdb.set_trace()
    #do the actual sorting
    if sorting == 't_price' or sorting == '-t_price':
        prods=prods.order_by(sorting)
        results=list(chain(prods,bevs))
    else:
        rev=sorting[0] is '-'
        results=sorted(chain(prods,bevs),key=lambda x: x.get('name'),reverse=rev)

    
    paginator=Paginator(results,show)
    paginated_results=[]
    try:
        paginated_results = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        paginated_results = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        paginated_results = paginator.page(paginator.num_pages)
    
    for bev in paginated_results:
        bev['price']=(' $ '+str(bev['t_price']))  if 't_price' in bev else ' Sold Out'
    return render(request,'spicy/category.html',{'beverages':paginated_results, 'drink_type':type1,})
        
    
    
def category_old(request,type1):
    sorting=request.REQUEST.get('sort_by')
    page=request.REQUEST.get('page',DEFAULT_PAGE)
    show=request.REQUEST.get('show',DEFAULT_LIMIT)
    show=show if show in {10,20,50,100} else DEFAULT_LIMIT
    if show>100:
        show=100
    bevs=Beverages.objects.filter(type1=type1)
    #optimized sorting for names and
    bevs=orderBy(bevs,sorting)
    paginator=Paginator(bevs,show)
    try:
        bevs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        bevs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        bevs = paginator.page(paginator.num_pages)
    
    for bev in bevs:
        best=Products.objects.filter(beverage=bev,sells__isnull=False).aggregate(max_price=Max('price'))
        bev.price='$ '+str('Sold Out' if best['max_price'] is  None else best['max_price'])
    variables={}
    variables['beverages']=bevs
    variables['drink_type']=type1
        #filter(product_beverage_name=bev.name).order_by('price')

    return render(request,'spicy/category.html',{'beverages':bevs, 'drink_type':type1,})
    #return render(request,'spicy/category.html',variables)

def product(request,beverage_id):
    bev=Beverages.objects.get(id=beverage_id)
    prods=Products.objects.filter(beverage=bev,sells__isnull=False)
    best=prods.aggregate(max_price=Max('price'))
    bev.price='$ '+str('Sold Out' if best['max_price'] is None else best['max_price'])
    all_together_now=prods.values('size').annotate(max_price=Max('price')).values_list('size','max_price')
    group_dict={}
    for group in all_together_now:
        group_dict[group[0]]=group[1]
        
    return render(request,'spicy/product.html',{'bev':bev,'sizes':group_dict},)

def compare_add(request,beverage_id):
    compares=request.session.get('compare_items',[])
    next=request.REQUEST.get('next',None)
    render_message={}
    if len(compares)>5:
        render_message['compare_message']="Uhhh...:  You can't compare more than 5 items " %(Beverages.objects.get(id=beverage_id).name)
    elif(beverage_id in compares):
        render_message['compare_message']="Uhhh...:  %s to your " %(Beverages.objects.get(id=beverage_id).name)
    if not render_message.get('compare_message',None):
        compares.append(beverage_id)
        request.session['compare_items']=compares
        render_message['compare_message']="Success: You have added %s to your product" %(Beverages.objects.get(id=beverage_id).name)
    next=request.REQUEST.get('next',None)
    
    if next:
        return redirect(next)
    return render(request, 'spicy/index.html',render_message)

def compare_delete(request,beverage_id):
    compares=request.session.get('compare_items',[])
    compares.remove(beverage_id)
    request.session['compare_items']=compares
    next=request.REQUEST.get('next',None)
    if next:
        return redirect(next)
    return render(request,'spicy/compare.html',{'compare_message':"Success: You have Removed %s from your product" %(Beverages.objects.get(id=beverage_id).name),'compare_beverages':compares})
    

def compare(request):
    bevs=[Beverages.objects.get(id=an_id) for an_id in request.session.get('compare_items',[])]
    return render(request,'spicy/compare.html',{'compare_beverages':bevs},)

def add_to_cart(request,beverage_id):
    cart_items=request.session.get('cart_items',[])
    render_message={}
    if beverage_id not in cart_items:
        cart_items.append(beverage_id)
        request.session['cart_items']=cart_items
        render_message['cart_message']="Sucess: You have added %s to your cart" %(Beverages.objects.get(id=beverage_id).name)
        
    next=request.REQUEST.get('next',None)
    if next:
        return redirect(next)
    return render(request,'spicy/index.html',render_message)
        
    
def remove_from_cart(request,beverage_id):
    cart_items=request.session.get('cart_items',[])
    if beverage_id  in compares:
        cart_items.append(beverage_id)
        request.session['cart_items']=cart_items
        render_message['cart_message']="Sucess: You have removed %s from your cart" %(Beverages.obects.get(id=beverage_id).name)
    next=request.REQUEST.get('next',None)
    if next:
        return redirect(next)
    return render(request,'spicy/index.html',render_message)

def cart(request):
        bevs=[Beverages.objects.get(id=an_id) for an_id in request.session.get('cart_items',[])]
        return render(request,'spicy/cart.html',{'cart_beverages':bevs})
