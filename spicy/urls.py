from django.conf.urls.defaults import patterns, url
from django.views.generic.simple import direct_to_template
from spicy import views

urlpatterns = patterns('',
                       #url(r'^',direct_to_template,{'template': 'spicy/index.html'},name='index'),
                       url(r'^index/',direct_to_template,{'template': 'spicy/index.html'},name='index'),
                       url(r'^checkout/',direct_to_template,{'template': 'spicy/checkout.html'},name='checkout'),
                       url(r'^404/',direct_to_template,{'template': 'spicy/404.html'},name='404'),
                       url(r'^account/',direct_to_template,{'template': 'spicy/account.html'},name='account'),
                       url(r'^cart/',views.cart,name='cart'),
                       url(r'^cart_add/(?P<beverage_id>\w+)',views.add_to_cart,name='add_to_cart'),
                       url(r'^cart_remove/(?P<beverage_id>\w+)',views.remove_from_cart,name='remove_from_cart'),
                       url(r'^compare/',views.compare,name='compare'),
                       url(r'^compare_add/(?P<beverage_id>\w+)/$',views.compare_add,name='compare_add'),
                       url(r'^compare_delete/(?P<beverage_id>\w+)/$',views.compare_add,name='compare_delete'),
                       url(r'^contact/',direct_to_template,{'template': 'spicy/contact.html'},name='contact'),
                       url(r'^wish/',direct_to_template,{'template': 'spicy/wish.html'},name='wish'),
                       url(r'^category/(?P<type1>\w+)/$',views.category,name='category'),
                       url(r'^product/(?P<beverage_id>\w+)/$',views.product,name='product'),
                       #url(r'catgeory/(?P<type1>\w+)/$',direct_to_template,{'template': 'spicy/category.html'},name='category'),
                       #url(r'^product/',direct_to_template,{'template': 'spicy/product.html'},name='product'),
                       )
