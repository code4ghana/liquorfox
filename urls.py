from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()



urlpatterns=patterns('',
                     #temporary home page to show that this works
                     url(r'',include('social_auth.urls')),
                     url(r'^maintenance/',direct_to_template, {'template': 'landingpage/maintenance.html'}),
#                     url(r'^registration/login/',direct_to_template, {'template': 'registration/login.html'},name='login'),
                     url(r'^main/drinks_search/','listings.views.ajax_beverage',name='drinks_search'),
                     url(r'^spicy/',include('spicy.urls')),
                     (r'^admin/doc/', include('django.contrib.admindocs.urls')),                     
                     url(r'^admin/', include(admin.site.urls)),
                     )
#this makes the static files (images, css, js, etc) work when in local development
urlpatterns += staticfiles_urlpatterns()

