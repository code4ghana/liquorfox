from django.shortcuts import render_to_response, get_list_or_404,redirect,get_object_or_404
from django.utils import simplejson
from django.http import HttpResponse
from liquorfox_core.models import Beverages, Bars
from django.template import RequestContext


#from django.contrib.auth.forms import AuthenticationForm
def is_over_18(request):
	return request.session['over_18']
def index(request):
	top_ten_beverages=get_list_or_404(Beverages)[:10]
# 	if request.method == 'POST' and 'is_top_login_form' in request.POST:

# 		# validate the form
# 		form = AuthenticationForm(data=request.POST)
# 		if form.is_valid():

# 			# log the user in
# 			from django.contrib.auth import login
# 			login(request, form.get_user())

# 			# if this is the logout page, then redirect to /
# 			# so we don't get logged out just after logging in
# 			if '/account/logout/' in request.get_full_path():
# 				return HttpResponseRedirect('/')

# 			else:
# 				form = AuthenticationForm(request)
	
# #	form =BeverageAutoComplete(source='ajax_beverage')
	data={'beverages':top_ten_beverages}
	return render_to_response('listings/index.html',data,context_instance=RequestContext(request))

		

def seller(request, seller_id):
	p=get_list_or_404(Bars,pk=seller_id)
	return render_to_response('listings/bar.html',{'bar':p})


def beverage_id(request, beverage_id):
	p=get_list_or_404(Beverages,pk=beverage_id)
	return render_to_response('listings/results.html',{'beverages':p})

def beverage(request, beverage_name):
	if not request.POST:
		return render_to_response('listings/results.html',{})
	search=beverage_name
	if search:
	 	bevs=get_list_or_404(Beverages,name__icontains=search)[:10]
	 	#response=dic((b.name,b.asJson()) for b in bevs)
		return render_to_response('listings/results.html',{'beverages':bevs})

def ajax_beverage(request,search):
	limit=20
	response=[]
	if not request.is_ajax():
		return render_to_response('spicy/index.html',{})
	if search and len(search)>3:
		bev=get_object_or_404(Beverages,name_icontains=search)
		friends=get_list_or_404(Beverages,type1=bev.type1)[:limit]
		friends.append(bev)
		response=[(content_flow_response(x), x.id) for x in friends]
	json=simplejson.dumps(response)
	return json

def content_flow_response(beverage):
	html="""<div class="item"> <img class="content" src="%(image)s"/>
	<div class="caption">%(name)s<br/><a href="">by %(type)s</a></div></div>"""
	ret=html% {'image':beverage.pic_url, 'name':beverage.name,'type':beverage.type1}
	return ret

def ajax_beverages_list(request):
	results = []
	limit=10
	
	if request.method == "GET":
		# import pdb
		# pdb.set_trace()
		if request.GET.has_key(u'query'):
			value = request.GET[u'query']
			# Ignore queries shorter than length 3
			if len(value) > 0:
				model_results=get_list_or_404(Beverages,name__icontains=value)[:limit]
				results = {'query':value }
				results['suggestions']=[bev.__unicode__() for bev in model_results]
	json = simplejson.dumps(results)
	return HttpResponse(json, mimetype='application/json')
	

def beverage_ajax(request):
	results = []
	limit=10
	
	if request.method == "GET":
		# import pdb
		# pdb.set_trace()
		if request.GET.has_key(u'query'):
			value = request.GET[u'query']
			# Ignore queries shorter than length 3
			if len(value) > 2:
				model_results=get_list_or_404(Beverages,name__icontains=value)[:limit]
				results = [ (x.__unicode__(), x.id) for x in model_results ]
	json = simplejson.dumps(results)
	return HttpResponse(json, mimetype='application/json')

def landing(request):
	if request.session.get('over_18',False):
		return redirect('main')
	if request.method=='POST':
		email=request.POST['email']
		address=request.POST['address']
		reqister=request.POST['register']
		request.session['over_18']=True
		return render_to_response('landingpage/main.html',context_instance=RequestContext(request))
	return render_to_response('landingpage/index.html',context_instance=RequestContext(request))


def main(request):
	if not request.session.get('over_18',False):
		return redirect('/');
	return render_to_response('landingpage/main.html',context_instance=RequestContext(request))

	
