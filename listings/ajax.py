from django.utils import simplejson
from dajaxice.decorators import dajaxice_register
from listings.models import Beverages

@dajaxice_register
def ajax_beverage_proxy(request,drink_name):
    ret={}
    bev=Beverages.objects.filter(name__icontains=drink_name)
    if(len(bev)>0):
        found=bev[0]
        friends=list(Beverages.objects.filter(type1=found.type1).exclude(name__exact=found.name)[:50])
        friends.append(found)
        html_new="""<img class="item" src="%(image)s" title="%(name)s"/>"""
        html="""<div class="item" href=%(link)s>
<img class="content" src="%(image)s"/>
<div class="caption">%(name)s <br/><a href="%(link)s">%(type1)s</a></div>
<div class="label">%(name)s</div>
</div>"""
        ret=[html%{'image':drink.pic_url, 'name':drink.name,'link':drink.pic_url,'type1':drink.type1} for drink in friends]
        ret_new=[html_new%{'image':drink.pic_url, 'name':drink.name} for drink in friends]
    return simplejson.dumps(ret)
