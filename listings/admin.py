from listings.models import Beverages, Bars, Stores,  Votables, Sells, Sellers, Products
from django.contrib import admin
admin.site.register(Sellers)
admin.site.register(Votables)
admin.site.register(Beverages)
admin.site.register(Bars)
admin.site.register(Stores)
admin.site.register(Sells)
admin.site.register(Products)
    
