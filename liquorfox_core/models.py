from django.db import models
from django.contrib.localflavor.us import models as us_models
#from profiles.models import Profile



        
class Beverages(models.Model):
	class Meta:
		verbose_name_plural='Baverages'
		verbose_name='Beverage'
	
	name=models.CharField(unique=True,max_length=100)
	origin=models.CharField(max_length=100,blank=True)
	color=models.CharField(max_length=30,blank=True)
	pic_url=models.URLField()
	description=models.TextField(blank=True)
	type1=models.CharField( max_length=100)
	type2=models.CharField( blank=True, max_length=100)
	phase1=models.CharField(max_length=10,blank=True)
	phase2=models.CharField(max_length=10, blank=True)
	proof=models.IntegerField(verbose_name='proof',default=0,blank=True)
	alcohol=models.IntegerField(verbose_name='alcohol %',default=0,blank=True)
	min_age=models.IntegerField(default=0,blank=True)
	max_age=models.IntegerField(default=0,blank=True)
	distery_location=models.CharField(max_length=100,blank=True)
	popular_mix1=models.TextField('popular_mix1', blank=True)
	popular_mix2=models.TextField('popular_mix2', blank=True)
	parent_company=models.CharField(max_length=100, blank=True)
	

	def __unicode__(self):
		return self.name

	def is_available(self):
		return Sells.objects.filter(product__name__exact=self.name).exists()

	def asJson(self,ALL=True, **kwargs):
		if ALL:
			dump={'name':self.name,
			      'origin':self.origin,
			      'color':self.color,
			      'pic':self.pic_url,
			      'description':self.description,
			      'type1':self.type1,
				}
			return dump
		
		dump={}
		if 'name' in kwargs:
			dump['name']=self.name
		if 'origin' in kwargs:
			dump['origin']=self.origin
		if 'color' in  kwargs:
			dump['color']=self.color
		if 'pic' in kwargs:
			dump['pic']=self.pic_url
		if 'description' in kwargs:
			dump['description']=self.description
		if 'type1' in kwargs:
			dump['type1']=self.type1
		return dump

class Products(models.Model):
	class Meta:
		verbose_name_plural='Products'
		verbose_name='Product'

	barcode=models.CharField(max_length=50,unique=True)
	beverage=models.ForeignKey(Beverages)
	size=models.IntegerField(help_text="Please input the size in Milliliters e.x. 200 for 200ml",validators=[lambda x:x>0,])
	price=models.DecimalField(max_digits=10,decimal_places=2,help_text="Please input the price in Dollars e.x. 200.00 for $200.00",validators=[lambda x:x>0,])

	def __unicode__(self):
		return str(self.size)+'ml ' +self.beverage.__unicode__() +' for $'+str(self.price)
	

class Sellers(models.Model):
	class Meta:
		verbose_name_plural='Sellers'
		verbose_name='Sellers'
		
	def __unicode__(self):
		if hasattr(self,'bars'):
			return self.bars.__unicode__()
		elif hasattr(self,'stores'):
			return self.stores.__unicode__()
		
		return 'Seller'
		
	
class Votables(models.Model):
	class Meta:
		verbose_name_plural='Votables'
		verbose_name='Votable'
		

class Bars(Sellers):
	
	class Meta:
		verbose_name_plural='Bars'
		verbose_name='Bar'

	name=models.CharField(max_length=100)
	email=models.CharField(max_length=50)
	music=models.CharField(max_length=100)
	crowd=models.CharField(max_length=50)

	def __unicode__(self):
		return self.name+" (Bar)"


class Stores(Sellers):
	
	class Meta:
		verbose_name_plural='Stores'
		verbose_name='Store'
		
	name=models.CharField(max_length=100,verbose_name="Store Name")
	email=models.CharField(max_length=50,)
	#music=models.CharField(max_length=100)
	
	address=models.CharField(max_length=100,blank=True,verbose_name="Address")
	state=us_models.USStateField(blank=True,verbose_name="State")
	primary_phone=us_models.PhoneNumberField(blank=True,verbose_name="Primary Phone Number")
	zipcode=models.CharField(max_length=50,blank=True, verbose_name="Zip")
	#crowd=models.CharField(max_length=50)

	def __unicode__(self):
		return self.name+" (Store)"
	

# class Votes(models.Model):
# 	class Meta:
# 		verbose_name_plural='Votes'
# 		verbose_name='Vote'

# 	voter=models.ForeignKey(Profile)
# 	#item=models.ForeignKey(Votables)
# 	VOTE_CATHEGORIES=(
# 		('B', 'Bars'),
# 		('S', 'Stores'),
# 		)
# 	cat=models.CharField(max_length=1,choices=VOTE_CATHEGORIES)
# 	def vote_validate(value):
# 		if not isinstance(value,(int,long)) or value not in range(0,6):
# 			raise ValidationError(u'%s Please provide a valid vote' % value)

# 	vote=models.IntegerField(default=5,validators=[vote_validate])
	

class Sells(models.Model):
	class Meta:
		verbose_name_plural='Sells'
		verbose_name='Sells'
		#unique_together=('seller','product')
	seller=models.ForeignKey(Sellers)
	product=models.ForeignKey(Products)


	def __unicode__(self):
		return "%s sells %s" %(self.seller.__unicode__(),self.product.__unicode__())
	
	def save(self,*args,**kwargs):
		exists=Sells.objects.get(seller=self.seller,product=self.product)
		if exists:
			return
		super(Sells,self).save(*args,**kwargs)

		
