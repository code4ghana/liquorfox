# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Beverages'
        db.create_table('liquorfox_core_beverages', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('origin', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('pic_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('type1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('type2', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('phase1', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('phase2', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('proof', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('alcohol', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('min_age', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('max_age', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('distery_location', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('popular_mix1', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('popular_mix2', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('parent_company', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('liquorfox_core', ['Beverages'])

        # Adding model 'Products'
        db.create_table('liquorfox_core_products', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('barcode', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('beverage', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['liquorfox_core.Beverages'])),
            ('size', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('liquorfox_core', ['Products'])

        # Adding model 'Sellers'
        db.create_table('liquorfox_core_sellers', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('liquorfox_core', ['Sellers'])

        # Adding model 'Votables'
        db.create_table('liquorfox_core_votables', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('liquorfox_core', ['Votables'])

        # Adding model 'Bars'
        db.create_table('liquorfox_core_bars', (
            ('sellers_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['liquorfox_core.Sellers'], unique=True, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('music', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('crowd', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('liquorfox_core', ['Bars'])

        # Adding model 'Stores'
        db.create_table('liquorfox_core_stores', (
            ('sellers_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['liquorfox_core.Sellers'], unique=True, primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('music', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('crowd', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('liquorfox_core', ['Stores'])

        # Adding model 'Sells'
        db.create_table('liquorfox_core_sells', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('seller', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['liquorfox_core.Sellers'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['liquorfox_core.Products'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal('liquorfox_core', ['Sells'])


    def backwards(self, orm):
        # Deleting model 'Beverages'
        db.delete_table('liquorfox_core_beverages')

        # Deleting model 'Products'
        db.delete_table('liquorfox_core_products')

        # Deleting model 'Sellers'
        db.delete_table('liquorfox_core_sellers')

        # Deleting model 'Votables'
        db.delete_table('liquorfox_core_votables')

        # Deleting model 'Bars'
        db.delete_table('liquorfox_core_bars')

        # Deleting model 'Stores'
        db.delete_table('liquorfox_core_stores')

        # Deleting model 'Sells'
        db.delete_table('liquorfox_core_sells')


    models = {
        'liquorfox_core.bars': {
            'Meta': {'object_name': 'Bars', '_ormbases': ['liquorfox_core.Sellers']},
            'crowd': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'music': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'})
        },
        'liquorfox_core.beverages': {
            'Meta': {'object_name': 'Beverages'},
            'alcohol': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'distery_location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'min_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'origin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phase1': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'phase2': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'pic_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'popular_mix1': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'popular_mix2': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'proof': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'type1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'liquorfox_core.products': {
            'Meta': {'object_name': 'Products'},
            'barcode': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'beverage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Beverages']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'size': ('django.db.models.fields.IntegerField', [], {})
        },
        'liquorfox_core.sellers': {
            'Meta': {'object_name': 'Sellers'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'liquorfox_core.sells': {
            'Meta': {'object_name': 'Sells'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Products']"}),
            'seller': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Sellers']"})
        },
        'liquorfox_core.stores': {
            'Meta': {'object_name': 'Stores', '_ormbases': ['liquorfox_core.Sellers']},
            'crowd': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'music': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'})
        },
        'liquorfox_core.votables': {
            'Meta': {'object_name': 'Votables'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['liquorfox_core']