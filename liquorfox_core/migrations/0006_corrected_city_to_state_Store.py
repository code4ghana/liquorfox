# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Stores.city'
        db.delete_column('liquorfox_core_stores', 'city')

        # Adding field 'Stores.state'
        db.add_column('liquorfox_core_stores', 'state',
                      self.gf('django.contrib.localflavor.us.models.USStateField')(default='', max_length=2, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Stores.city'
        db.add_column('liquorfox_core_stores', 'city',
                      self.gf('django.contrib.localflavor.us.models.USStateField')(default='', max_length=2, blank=True),
                      keep_default=False)

        # Deleting field 'Stores.state'
        db.delete_column('liquorfox_core_stores', 'state')


    models = {
        'liquorfox_core.bars': {
            'Meta': {'object_name': 'Bars', '_ormbases': ['liquorfox_core.Sellers']},
            'crowd': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'music': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'})
        },
        'liquorfox_core.beverages': {
            'Meta': {'object_name': 'Beverages'},
            'alcohol': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'distery_location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'min_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'origin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phase1': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'phase2': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'pic_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'popular_mix1': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'popular_mix2': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'proof': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'type1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'liquorfox_core.products': {
            'Meta': {'object_name': 'Products'},
            'barcode': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'beverage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Beverages']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'size': ('django.db.models.fields.IntegerField', [], {})
        },
        'liquorfox_core.sellers': {
            'Meta': {'object_name': 'Sellers'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'liquorfox_core.sells': {
            'Meta': {'object_name': 'Sells'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Products']"}),
            'seller': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Sellers']"})
        },
        'liquorfox_core.stores': {
            'Meta': {'object_name': 'Stores', '_ormbases': ['liquorfox_core.Sellers']},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'primary_phone': ('django.contrib.localflavor.us.models.PhoneNumberField', [], {'max_length': '20', 'blank': 'True'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'}),
            'state': ('django.contrib.localflavor.us.models.USStateField', [], {'max_length': '2', 'blank': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'liquorfox_core.votables': {
            'Meta': {'object_name': 'Votables'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['liquorfox_core']