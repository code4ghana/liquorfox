# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Stores.crowd'
        db.delete_column('liquorfox_core_stores', 'crowd')

        # Deleting field 'Stores.music'
        db.delete_column('liquorfox_core_stores', 'music')

        # Adding field 'Stores.address'
        db.add_column('liquorfox_core_stores', 'address',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Stores.city'
        db.add_column('liquorfox_core_stores', 'city',
                      self.gf('django.contrib.localflavor.us.models.USStateField')(default='', max_length=2, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Stores.crowd'
        db.add_column('liquorfox_core_stores', 'crowd',
                      self.gf('django.db.models.fields.CharField')(default='none', max_length=50),
                      keep_default=False)

        # Adding field 'Stores.music'
        db.add_column('liquorfox_core_stores', 'music',
                      self.gf('django.db.models.fields.CharField')(default='none', max_length=100),
                      keep_default=False)

        # Deleting field 'Stores.address'
        db.delete_column('liquorfox_core_stores', 'address')

        # Deleting field 'Stores.city'
        db.delete_column('liquorfox_core_stores', 'city')


    models = {
        'liquorfox_core.bars': {
            'Meta': {'object_name': 'Bars', '_ormbases': ['liquorfox_core.Sellers']},
            'crowd': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'music': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'})
        },
        'liquorfox_core.beverages': {
            'Meta': {'object_name': 'Beverages'},
            'alcohol': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'distery_location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'min_age': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'origin': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent_company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'phase1': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'phase2': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'pic_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'popular_mix1': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'popular_mix2': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'proof': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'type1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'liquorfox_core.products': {
            'Meta': {'object_name': 'Products'},
            'barcode': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'beverage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Beverages']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'size': ('django.db.models.fields.IntegerField', [], {})
        },
        'liquorfox_core.sellers': {
            'Meta': {'object_name': 'Sellers'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'liquorfox_core.sells': {
            'Meta': {'object_name': 'Sells'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Products']"}),
            'seller': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['liquorfox_core.Sellers']"})
        },
        'liquorfox_core.stores': {
            'Meta': {'object_name': 'Stores', '_ormbases': ['liquorfox_core.Sellers']},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'city': ('django.contrib.localflavor.us.models.USStateField', [], {'max_length': '2', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'sellers_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['liquorfox_core.Sellers']", 'unique': 'True', 'primary_key': 'True'})
        },
        'liquorfox_core.votables': {
            'Meta': {'object_name': 'Votables'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['liquorfox_core']