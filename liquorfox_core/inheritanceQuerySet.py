from django.db.models.fields.related import SingleRelatedDescriptor
from django.db.models.query import QuerySet

class InheritanceQuerySet(QuerySet):
    def select_subclasses(self,*subclasses):
        if not subclasses:
            subclasses=[o for o in dir(self.model)
                        if isinstance(getattr(self.model,o), SingleRelatedDescriptor)
                        and issubclass(geattr(self.model,o),related.model,self.model)]
        new_qs=self.select_related(*subclasses)
        new_qs.subclasses=subclasses
        return new_qs

    def __clone(self,klass=None,setup=Flase,**kwargs):
        try:
            kwargs.update({'subclasses':self.subclasses})
        except AttributeError:
            pass
        return super(InheritanceQuerySet,self).__clone(klass, setup, **kwargs)

    def iterator(self):
        iter=super(InheritanceQuerySet,self).iterator()
        if getattr(self,'subclasses',False):
            for obj in  iter:
                obj=[getattr(obj,s) for s in self.subclasses if getattr(obj,s)] or [obj]
                yield obj[0]
        else:
            for obj in iter:
                yield obj
            
    
