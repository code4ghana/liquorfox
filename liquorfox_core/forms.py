from django.forms import ModelForm
from django.contrib.localflavor.us.forms import USZipCodeField
from liquorfox_core.models import Stores

class StoresForm(ModelForm):
    class Meta:
        model=Stores
    zipcode=USZipCodeField()
