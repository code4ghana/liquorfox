from liquorfox_core.models import Beverages, Bars, Stores,  Votables, Sells, Sellers, Products
from django.contrib import admin
from liquorfox_core.forms import StoresForm

class StoresAdmin(admin.ModelAdmin):
    form=StoresForm

admin.site.register(Sellers)
admin.site.register(Votables)
admin.site.register(Beverages)
admin.site.register(Bars)
admin.site.register(Stores,StoresAdmin)
admin.site.register(Sells)
admin.site.register(Products)
    
