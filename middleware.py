from django.http import HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm

class LoginFormMiddleware(object):
    def process_request(self, request):
        
        # if the top login form has been posted
        if request.method == 'POST' and 'login_form' in request.POST:
            
            # validate the form
            form = AuthenticationForm(prefix='login_form', data=request.POST)
            if form.is_valid():
                
                # log the user in
                from django.contrib.auth import login
                login(request, form.get_user())
    
                # if this is the logout page, then redirect to /
                # so we don't get logged out just after logging in
                if '/account/logout/' in request.get_full_path():
                    return HttpResponseRedirect('/')
                
        else:
            form=AuthenticationForm(request,prefix='login_form')
            #if the user is coming from the
            # attach the form to the request so it can be accessed within the templates
        request.login_form = form

class MaintenanceMiddleware(object):
    def process_request(self,request):
        from django.conf import settings
        from django.http import HttpResponseRedirect
        is_login = request.path in (
            settings.LOGIN_URL,
            settings.LOGIN_REDIRECT_URL,
            settings.LOGOUT_URL,
            settings.ADMIN_LOGIN_URL,
            settings.ADMIN_LOGOUT_URL,
            settings.MAINTENANCE_PATH,
            '/registration/login/',)

        
        if ((not is_login) and settings.MAINTENANCE and (not
                                                         request.user.is_authenticated())):
            return HttpResponseRedirect(settings.MAINTENANCE_PATH)
        return None
