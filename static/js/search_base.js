function ImageFlow(){
    this.CWIDTH;
    this.CHEIGHT;
    this.CGAP = 10;
    this. CXSPACING;
    this. CYSPACING;

    this.currentCell = -1;
    this.cells = [];

    this.currentTimer = null;

    this.dollyId= "#dolly";
    this.cameraId = "#camera";
    this.stackId="#stack";
    this.rstackId="#rstack";
    this.mirrorId="#mirror";
    this.magnifyMode = false;

    this.zoomTimer = null;
    this.keys = { left: false, right: false, up: false, down: false };
    this.keymap = { 37: "left", 38: "up", 39: "right", 40: "down" };
    this.page = 1;
    this.loading = true;
    this.keytimer = null;

	

}

ImageFlow.prototype.snowstack_init=function()
{
    this.CHEIGHT = Math.round(window.innerHeight / 5);
    this.CWIDTH = Math.round(this.CHEIGHT * 300 / 180);
    this.CXSPACING = this.CWIDTH + this.CGAP;
    this.CYSPACING = this.CHEIGHT + this.CGAP;

    jQuery(this.mirrorId)[0].style.webkitTransform = "scaleY(-1.0) " + this.translate3d(0, - this.CYSPACING * 6 - 1, 0);
};

ImageFlow.prototype.translate3d=function(x, y, z)
{
    return "translate3d(" + x + "px, " + y + "px, " + z + "px)";
};

ImageFlow.prototype.cameraTransformForCell=function(n)
{
    var x = Math.floor(n / 3);
    var y = n - x * 3;
    var cx = (x + 0.5) * this.CXSPACING;
    var cy = (y + 0.5) * this.CYSPACING;

    if (this.magnifyMode)
    {
	return this.translate3d(-cx, -cy, 180);
    }
    else
    {
	return this.translate3d(-cx, -cy, 0);
    }	
};


ImageFlow.prototype.refreshImage=function(elem, cell)
{
    if (cell.iszoomed)
    {
	return;
    }

    if (this.zoomTimer)
    {
	clearTimeout(zoomTimer);
    }
    
    var zoomImage = jQuery('<img class="zoom"></img>');

    this.zoomTimer = setTimeout(function ()
				{
				    zoomImage.load(function ()
						   {
						       this. layoutImageInCell(zoomImage[0], cell.div[0]);
						       jQuery(elem).replaceWith(zoomImage);
						       cell.iszoomed = true;
						   });

				    zoomImage.attr("src", cell.info.zoom);

				    zoomTimer = null;
				}, 2000);
};

ImageFlow.prototype.layoutImageInCell=function(image, cell)
{
    var iwidth = image.width;
    var iheight = image.height;
    var cwidth = jQuery(cell).width();
    var cheight = jQuery(cell).height();
    var ratio = Math.min(cheight / iheight, cwidth / iwidth);
    
    iwidth *= ratio;
    iheight *= ratio;

    image.style.width = Math.round(iwidth) + "px";
    image.style.height = Math.round(iheight) + "px";

    image.style.left = Math.round((cwidth - iwidth) / 2) + "px";
    image.style.top = Math.round((cheight - iheight) / 2) + "px";
};

ImageFlow.prototype.updateStack=function(newIndex, newmagnifymode)
{
    if (this.currentCell == newIndex && this.magnifyMode == newmagnifymode)
    {
	return;
    }

    var oldIndex = this.currentCell;
    newIndex = Math.min(Math.max(newIndex, 0), this.cells.length - 1);
    this.currentCell = newIndex;

    if (oldIndex != -1)
    {
	var oldCell = this.cells[oldIndex];
	oldCell.div.attr("class", "cell fader view original");	
	if (oldCell.reflection)
	{
	    oldCell.reflection.attr("class", "cell fader view reflection");
	}
    }
    
    var cell = this.cells[newIndex];
    cell.div.addClass("selected");
    
    if (cell.reflection)
    {
	cell.reflection.addClass("selected");
    }

    this.magnifyMode = newmagnifymode;
    
    if (this.magnifyMode)
    {
	cell.div.addClass("magnify");
	this.refreshImage(cell.div.find("img")[0], cell);
    }

    jQuery(this.dollyId)[0].style.webkitTransform = this.cameraTransformForCell(newIndex);
    
    var currentMatrix = new WebKitCSSMatrix(document.defaultView.getComputedStyle(jQuery(this.dollyId)[0], null).webkitTransform);
    var targetMatrix = new WebKitCSSMatrix(jQuery(this.dollyId)[0].style.webkitTransform);
    
    var dx = currentMatrix.e - targetMatrix.e;
    var angle = Math.min(Math.max(dx / (this.CXSPACING * 3.0), -1), 1) * 45;

    jQuery(this.cameraId)[0].style.webkitTransform = "rotateY(" + angle + "deg)";
    jQuery(this.cameraId)[0].style.webkitTransitionDuration = "330ms";

    if (this.currentTimer)
    {
	clearTimeout(this.currentTimer);
    }
    
    this.currentTimer = setTimeout(jQuery.proxy(function ()
					{
					    jQuery(this.cameraId)[0].style.webkitTransform = "rotateY(0)";
					    jQuery(this.cameraId)[0].style.webkitTransitionDuration = "5s";
					},this), 330);
};

ImageFlow.prototype.snowstack_addimage=function (reln, info)
{
    var cell = {};
    var realn = this.cells.length;
    this.cells.push(cell);
    
    var x = Math.floor(realn / 3);
    var y = realn - x * 3;

    cell.info = info;

    cell.div = jQuery('<div class="cell fader view original" style="opacity: 0"></div>').width(this.CWIDTH).height(this.CHEIGHT);
    cell.div[0].style.webkitTransform = this.translate3d(x * this.CXSPACING, y * this.CYSPACING, 0);

    var img = document.createElement("img");
    var handler=function ()
    {
	this.layoutImageInCell(img, cell.div[0]);
	cell.div.append(jQuery('<a class="mover viewflat" href="' + cell.info.link + '" target="_blank"></a>').append(img));
	cell.div.css("opacity", 1);
    };
    jQuery(img).bind('load',jQuery.proxy(handler,this));
    
    img.src = info.thumb;

    jQuery(this.stackId).append(cell.div);

    if (y == 2)
    {
	cell.reflection = jQuery('<div class="cell fader view reflection" style="opacity: 0"></div>').width(this.CWIDTH).height(this.CHEIGHT);
	cell.reflection[0].style.webkitTransform = this.translate3d(x * this.CXSPACING, y * this.CYSPACING, 0);
	
	var rimg = document.createElement("img");
	handler=function ()
	{
	    this.layoutImageInCell(rimg, cell.reflection[0]);
	    cell.reflection.append(jQuery('<div class="mover viewflat"></div>').append(rimg));
	    cell.reflection.css("opacity", 1);
	};
	jQuery(rimg).bind('load',jQuery.proxy(handler,this));
	
	rimg.src = info.thumb;

	jQuery(this.rstackId).append(cell.reflection);
    }
};


ImageFlow.prototype.flickr=function(callback, page)
{
    var url = "http://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=60746a125b4a901f2dbb6fc902d9a716&per_page=21&extras=url_o,url_m,url_s&page=" + page + "&format=json&jsoncallback=?";
    var handler=function(data) 
    {
	var images = jQuery.map(data.photos.photo, function (item)
				{
				    return {
					thumb: item.url_s,
					zoom: 'http://farm' + item.farm + '.static.flickr.com/' + item.server + '/' + item.id + '_' + item.secret + '.jpg',
					link: 'http://www.flickr.com/photos/' + item.owner + '/' + item.id
				    };
				});
	jQuery.proxy(callback(images),this);
    }

    jQuery.getJSON(url,jQuery.proxy(handler,this));

};

ImageFlow.prototype.updatekeys=function(){
    var newcell = this.currentCell;
    if (this.keys.left)
    {
	/* Left Arrow */
	if (newcell >= 3)
	{
	    newcell -= 3;
	}
    }
    if (this.keys.right)
    {
	/* Right Arrow */
	if ((newcell + 3) < this.cells.length)
	{
	    newcell += 3;
	}
	else if (!this.loading)
	{
	    /* We hit the right wall, add some more */
	    this.page = this.page + 1;
	    this.loading = true;
	    this.flickr(function (images)
		   {
		       jQuery.each(images,jQuery.proxy(this.snowstack_addimage,this));
		       this.loading = false;
		   }, this.page);
	}
    }
    if (this.keys.up)
    {
	/* Up Arrow */
	newcell -= 1;
    }
    if (this.keys.down)
    {
	/* Down Arrow */
	newcell += 1;
    }

    this.updateStack(newcell, this.magnifyMode);
    
};

ImageFlow.prototype.keycheck=function()
{
    if (this.keys.left || this.keys.right || this.keys.up || this.keys.down)
    {
	if (this.keytimer === null)
	{
	    var delay = 330;
	    var doTimer = function ()
	    {
	    	this.updatekeys();
		this.keytimer = setTimeout(jQuery.proxy(doTimer,this), delay);
	    	delay = 60;
	    };
	    doTimer();
	}
    }
    else
    {
    	clearTimeout(this.keytimer);
    	this.keytimer = null;
    }
};

ImageFlow.prototype.toggleMagnify=function(){
    this.updateStack(this.currentCell,!this.magnifyMode);
};
